---
title: System doradczy dla lokalu gastronomicznego
subtitle: Systemy Ekspertowe 2015
author: Michał Liszcz, Michał Bigaj

date: 13-01-2016

documentclass: beamer

header-includes:
  - \graphicspath{{../beamerthemeAGH/}}
  - \usepackage{{../beamerthemeAGH/beamerthemeAGH}}

include-before:
  - \graphicspath{{./}}

---

## Plan prezentacji

0. Wprowadzenie - charakterystyka systemu

0. Dziedzina wiedzy - hipotezy, fakty, symptomy, ...

0. Funkcjonalność
    * tryb objaśniania
    * niepewności

0. Wybrane aspekty implementacji

0. Przykładowe sesje

0. Wyniki i wnioski

---

## Wprowadzenie

* system doradczy dla lokalu gastronomicznego
    * kuchnia "mieszana" - różne rodzaje potraw
    * różna charakterystyka klientów

* dobór potrawy dla kilenta
    * system zadaje kilentowi szereg pytań
    * na końcu proponuje potrawę (z określonym prawdopodobieństwem)

* może stanowić pomoc dla kelnera ...
* lub być używany bezpośrednio przez kilenta

---

## Dziedzina wiedzy - hipotezy

Hipotezy w postaci `suggest_food(X)`, gdzie `X`:

* schabowy
* hamburger
* kurczak po tajsku
* spaghetti
* pierogi z owocami
* ...

```prolog
suggest_food('chilli con carne') :-
    eats_impl(pork),
    eats_impl(rice),
    eats_impl(spicy)
```

---

## Dziedzina wiedzy - fakty pośrednie

Fakty pośrednie zależne od symptomów i innych faktów, przykładowo:

```prolog
eats_impl(rice) :-
    not(is_true('czy jest', 'uczulony na gluten')),
    eats_impl(boiled),
    is_true('czy lubi', 'dania z ryzem')
```

```prolog
eats_impl(spicy) :-
    is_true('czy lubi', 'ostre potrawy'),
    not(is_age(mlody))
```

---

## Dziedzina wiedzy - symptomy *(1/2)*

Symptomy pobierane są od użytkownika. Są cztery grupy:

* `is_true(X, Y)`
    * przykładowo: `is_true('czy lubi', 'smazone potrawy')`  
    * odpowiedź - współczynnik (nie)pewności z zakresu od -100 do 100  
    * *tak* (CF = 100), *nie* (CF = -100)

* `is_age(X)`
    * możliwe wartości: *stary*, *dorosly*, *mlody*
    * wartość wprowadzona ma CF = 100
    * pozostałe mają CF = -100

---

## Dziedzina wiedzy - symptomy *(2/2)*

* `is_day_of_week(X)`
    * możliwe wartości - dni tygodnia
    * wartość wprowadzona ma CF = 100
    * pozostałe mają CF = -100

* `is_religion(X)`
    * możliwe wartości: *chrzescijanin*, *muzulmanin*, *zyd*, *brak*
    * CF jak powyżej

---

## Funkcjonalność *(1/3)*

Została zrealizowana funkcjonalność podstawowa (bazowy system doradczy) oraz:

* moduł objaśniania
    * dwa warianty:
        * inspekcja aktualnego stosu Prolog-a
        * ręczne budowanie stosu

* niepewności
    * zgodne z modelem MYCIN

---

## Funkcjonalność *(2/3)*

### Moduł objaśniania

* odpowiada na pytania;
    * *why* - wyświetla stos reguł prowadzący do aktualnego pytania
    * *what* - wyświetla listę ustalonych symptomów i faktów pośrednich
    * *how* - jak *what* i dodatkowo wyświetla reguły dla znanych faktów

---

## Funkcjonalność *(3/3)*

### Niepewności

* oparte na modelu MYCIN (Stanford, 1970):  
  http://www-compsci.swan.ac.uk/~csphil/CS345/chapts5-9.pdf
    * `X :- A,B,C`
    * CF( X :- A,B,C ) = CF(X) * MIN( CF(A), CF(B), CF(C) )
    * CF w zakresie -100% do +100%

* implementacja zaadaptowana z przykładu:  
  http://www.cs.appstate.edu/~blk/cs4440/java/rule-prolog/exshell

---

## Wybrane aspekty implementacji

* Modułowa budowa
    * ładowanie modułów:
      ```prolog
      consult('food2.pro'),
      consult('rules2.pro'),
      consult('util2.pro').
      ```
    * uruchomienie projektu:
      ```prolog
      main().
      ```

* Seria kolejnych slajdów przedstawia wybrane szczegóły implementacyjne.

---

## Symptomy z dyskretnym zbiorem wartości

```prolog
:- dynamic([dynamic_cache/2]).

is_age(Age) :-
    dynamic_cache(age, _),   % if not stored goto next one
    !,                       % or cut
    dynamic_cache(age, Age). % and return stored value
```

---

```prolog
is_age(Age) :-
    !, write('podaj wiek [stary/mlody/dorosly]\n'),
    readln([Resp]),
    user_command(Resp),                     % handle user command
    validate_age(Resp) -> (
        assertz(dynamic_cache(age, Resp)),  % store result in dynamic rule
        Age = Resp                          % assert if is expected
    ); is_age(Age).

validate_age(Age) :-
    member(Age, [stary, mlody, dorosly]).
```

---

## Przeglądanie stosu Prolog-a

```prolog
ext_why() :-
    get_prolog_backtrace(100, Trace),
    maplist(map_stack2, Trace, MappedTrace),
    include(filter_stack, MappedTrace, FilteredTrace),
    dump_why(FilteredTrace).
```

---

```prolog
map_stack2(Frame, Out) :-
    Frame =.. [_, _Idx, WrappedClause, Pred],
    WrappedClause =.. [_, Clause, _],
    clause(Head, Body, Clause),
    clause_property(Clause, line_count(Line)),
    clause_property(Clause, file(File)),
    % write('\n'),
    Out = [Pred, Clause, Head, Body, Line, File].
```

---

## Rekurencyjny ewaluator

```prolog
% Case 1: truth value of goal is already known
% Case 2: negated goal
% Case 3: conjunctive goals

% Case 4: backchain on a rule in knowledge base
solve(Goal, CF, Rules, Threshold) :-
  % write('solve backchain '), write(Goal), nl,
  rule((Goal :- (Premise)), CF_rule),
  solve(Premise, CF_premise,
    [rule((Goal :- Premise), CF_rule)|Rules], Threshold),
  rule_cf(CF_rule, CF_premise, CF),
  above_threshold(CF, Threshold),
  assertz(known_rules_single([Goal, Premise, CF])).
```

---

```prolog
rule((
  suggest_food('nalesniki z serem') :-
      eats_impl(sweet),
      eats_impl(fried)
  ), 100).
```

```prolog
solve(suggest_food(Food), CF, [], 20),
write('Food '), write(Food), write(' with CF '), write(CF).
```

---

## Przykładowa sesja *(1/2)*

```prolog
?- main().
czy jest pan/i wegetarianin? [-100,100]|y|n
|: n
jaki jest dzisiaj dzien tygodnia?
[poniedzialek/wtorek/sroda/czwartek/piatek/sobota/niedziela]
|: sobota
jaka religie wyznajesz?
[chrzescijanin/zyd/muzulmanin/brak]
|: brak
czy lubi pan/i smazone potrawy? [-100,100]|y|n
|: 90
czy ma pan/i podwyzszony cholesterol? [-100,100]|y|n
```

---

## Przykładowa sesja *(2/2)*

```prolog
|: -75
czy jest pan/i bardzo glodny? [-100,100]|y|n
|: 100
podaj wiek [stary/mlody/dorosly]
|: stary
czy jest pan/i mezczyzna? [-100,100]|y|n
|: 100
czy jest pan/i w pospiechu? [-100,100]|y|n
|: n
suggest_food(schabowy) was concluded with certainty 71.25

Proponuje panu/i schabowy
Z pewnoscia 71.25 %
true.
```

---

## Przykładowa sesja - 2 *(1/2)*

```prolog
?- main().
czy jest pan/i wegetarianin? [-100,100]|y|n
|: 90
czy jest pan/i w pospiechu? [-100,100]|y|n
|: n
czy lubi pan/i kuchnie azjatycka? [-100,100]|y|n
|: n
```

---

## Przykładowa sesja - 2 *(2/2)*

```prolog
czy jest pan/i uczulony na cukier? [-100,100]|y|n
|: -70
czy lubi pan/i gotowane potrawy? [-100,100]|y|n
|: y
suggest_food(pierogi z owocami) was concluded with certainty 66.5

Proponuje panu/i pierogi z owocami
Z pewnoscia 66.5 %
true.
```

---

## Objaśnianie - Why

```prolog
jaka religie wyznajesz?
[chrzescijanin/zyd/muzulmanin/brak]
|: why
	is_religion(muzulmanin)
		eats_impl(pork) :: 95
			eats_impl(meat)
			not(is_religion(muzulmanin))
		suggest_food(schabowy) :: 100
			eats_impl(pork)
			eats_impl(fried)
			eats_impl(full)
			not(is_true(czy jest,w pospiechu))
jaka religie wyznajesz?
[chrzescijanin/zyd/muzulmanin/brak]
```

---

## Objaśnianie - What

```prolog
|: what
	SYMPTOMY ----------------------------------------------
		czy jest wegetarianin: -100
		day: sobota
	FAKTY POSREDNIE ---------------------------------------
		allowed_by_religion(meat) :: 95
		eats_impl(meat) :: 90.25
		eats_impl(pork) :: 85.7375
		eats_impl(fried) :: 71.25
		eats_impl(full) :: 95
```

---

## Objaśnianie - How

```prolog
|: how
	SYMPTOMY ----------------------------------------------
		czy jest wegetarianin: -100
		day: sobota
	FAKTY POSREDNIE ---------------------------------------
		allowed_by_religion(meat) :: 95
			not(is_day_of_week(piatek))
		eats_impl(meat) :: 90.25
			not(is_true(czy jest,wegetarianin))
			allowed_by_religion(meat)
		eats_impl(meat) :: 90.25
			not(is_true(czy jest,wegetarianin))
			allowed_by_religion(meat)
```

---

## Wyniki

* przetestowana implementacja
    * dobrana 'najlepsza' kolejność reguł
    * niepewności ustalone empirycznie

* zrealizowana dodatkowa funkcjonalność
    * tryb objaśniania
    * niepewności

---

## Koniec

Dziękujemy za uwagę
