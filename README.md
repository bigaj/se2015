# se2015

Projekt z przedmiotu Systemy Ekspertowe

*System doradczy dla klientów lokalu gastronomicznego*

## Quick start

```prolog
consult('food2.pro'), consult('rules2.pro'), consult('util2.pro').
main.
```

## Opis projektu

* 12 hipotez (`suggest_food(X)`)

* 19 faktów z pytaniami (symptomów)
  * `is_religion(X)`
  * `is_age(X)`
  * `is_day_of_week(X)`
  * `is_false('czy jest', 'w pospiechu')`
  * `is_false('czy jest', 'uczulony na gluten')`
  * `is_false('czy jest', wegetarianin)`
  * `is_true('czy lubi', 'smazone potrawy')`
  * `is_false('czy ma', 'podwyzszony cholesterol')`
  * `is_true('czy lubi', 'gotowane potrawy')`
  * `is_false('czy jest', 'uczulony na cukier')`
  * `is_true('czy lubi', 'kuchnie azjatycka')`
  * `is_false('czy jest', 'uczulony na sos sojowy')`
  * `is_true('czy lubi', 'ostre potrawy')`
  * `is_true('czy jest', 'bardzo glodny')`
  * `is_true('czy jest', mezczyzna)`
  * `is_true('czy jest', 'na diecie')`
  * `is_true('czy lubi', 'kuchnie srodziemnomorska')`
  * `is_true('czy chce', 'lekki posilek')`
  * `is_true('czy lubi', 'dania z ryzem')`

* 14 faktów wyprowadzanych przy użyciu reguł (faktów pośrednich)
  * `eats(X)`
    * pork
    * beef
    * meat
    * fried
    * boiled
    * sweet
    * asian
    * spicy
    * full
    * light
    * fit
    * pasta
    * rice
    * seafood
  * `allowed_by_religion(X)`

* 28 reguły (*count(':-')-1*)
