
:- module(util2, [
    % is_age/1,
    % is_true/2,
    % is_false/2,
    % is_day_of_week/1,
    % is_religion/1
    solve/2
]).

:- dynamic([
    dynamic_cache/2,
    dynamic_cache/3,
    dynamic_cache_single/1,
    known_rules/2,
    known_rules_single/1,

    known/2
]).

cleanup() :-
    retractall(dynamic_cache(_, _)),
    retractall(dynamic_cache(_, _, _)),
    retractall(dynamic_cache_single(_)),
    retractall(known_rules(_, _)),
    retractall(known_rules_single(_, _)),

    retractall(known(_, _)).

%% ============================================================================
%% evaluator
%% ============================================================================

%% ADAPTED FROM http://www.cs.appstate.edu/~blk/cs4440/java/rule-prolog/exshell

solve(Goal, CF) :-
  cleanup(),
  solve(Goal, CF, [], 20),
  write(Goal), write(' was concluded with certainty '), write(CF), nl,nl.

% Case 1: truth value of goal is already known
% solve(Goal, CF, _, Threshold) :-
%   known(Goal, CF),!,
%   above_threshold(CF, Threshold).

% Case 2: negated goal
solve( not(Goal), CF, Rules, Threshold) :- !,
  % write('solve not '), write(Goal), nl,
  invert_threshold(Threshold, New_threshold),
  solve(Goal, CF_goal, Rules, New_threshold),
  negate_cf(CF_goal, CF).

% Case 3: conjunctive goals
solve((Goal_1,Goal_2), CF, Rules, Threshold) :- !,
  % write('solve conj '), write(Goal_1), write(' && '), write(Goal_2), nl,
  solve(Goal_1, CF_1, Rules, Threshold),
  above_threshold(CF_1, Threshold),
  solve(Goal_2, CF_2, Rules, Threshold),
  above_threshold(CF_2, Threshold),
  and_cf(CF_1, CF_2, CF).

%Case 4: backchain on a rule in knowledge base
solve(Goal, CF, Rules, Threshold) :-
  % write('solve backchain '), write(Goal), nl,
  rule((Goal :- (Premise)), CF_rule),
  solve(Premise, CF_premise,
    [rule((Goal :- Premise), CF_rule)|Rules], Threshold),
  rule_cf(CF_rule, CF_premise, CF),
  above_threshold(CF, Threshold),
  % added
  assertz(known_rules_single([Goal, Premise, CF])).

%Case 5: fact assertion in knowledge base
solve(Goal, CF, _, Threshold) :-
  % write('solve assertion '), write(Goal), nl,
  rule(Goal, CF),
  above_threshold(CF, Threshold).

% Case 6: ask user
solve(Goal, CF, Rules, Threshold) :-
  % write('solve askable '), write(Goal), nl,
  askable(Goal),
  askuser(Goal, CF, Rules),!,
  % write('solved askable '), write(Goal),
  % write(' CF '), write(CF), write(' TH '), write(Threshold), nl,
  assert(known(Goal, CF)),
  above_threshold(CF, Threshold).

% Case 7A: All else fails, see if goal can be solved in prolog.
% solve(Goal, 100, _, _) :-
%  call(Goal).

askuser(Goal, CF, Rules) :-
  % nl,write('User query:'), write(Goal), nl,
  % write('? '),
  askuser_internal(Goal, CF, Goal, Rules).

%% ============================================================================
%% certainty factors
%% ============================================================================

%% ADAPTED FROM http://www.cs.appstate.edu/~blk/cs4440/java/rule-prolog/exshell

negate_cf(CF, Negated_CF) :-
  Negated_CF is -1 * CF.

and_cf(A, B, A) :- A =< B.
and_cf(A, B, B) :- B < A.

rule_cf(CF_rule, CF_premise, CF) :-
  CF is CF_rule * CF_premise/100.

above_threshold(CF, T) :-
  T >= 0, CF >= T.
above_threshold(CF, T) :-
  T < 0, CF =< T.

invert_threshold(Threshold, New_threshold) :-
  New_threshold is -1 * Threshold.

%% ============================================================================
%% handling user input
%% ============================================================================

askuser_internal(is_age(Age), 100, Goal, Stack) :-
  is_age(Age, Goal, Stack).
askuser_internal(is_age(_),  -100, _, _).

askuser_internal(is_day_of_week(DOW), 100, Goal, Stack) :-
  is_day_of_week(DOW, Goal, Stack).
askuser_internal(is_day_of_week(_),  -100, _, _).

askuser_internal(is_religion(Religion), 100, Goal, Stack) :-
  is_religion(Religion, Goal, Stack).
askuser_internal(is_religion(_),  -100, _, _).

askuser_internal(is_true(Ask, What), CF, Goal, Stack) :-
  is_true_false(Ask, What, CF, Goal, Stack).

%% ============================================================================
%% asking for age
%% ============================================================================

is_age(Age, _, _) :-
    dynamic_cache(age, _), !, dynamic_cache(age, Age).

is_age(Age, Goal, Stack) :-
    !, write('podaj wiek [stary/mlody/dorosly]\n'),
    readln([Resp]),
    user_command(Resp, Goal, Stack),
    member(Resp, [stary, mlody, dorosly]) -> (
        assertz(dynamic_cache(age, Resp)),
        assertz(dynamic_cache_single([age, Resp])),
        Age = Resp
    ); is_age(Age, Goal, Stack).

%% ============================================================================
%% asking for y/n/CF
%% ============================================================================

is_true_false(Ask, What, CF, _, _) :-
    dynamic_cache(Ask, What, _),   % if not stored already, goto next one
    !,                             % or cut ...
    dynamic_cache(Ask, What, CF).  % and check if expected val is stored

is_true_false(Ask, What, CF, Goal, Stack) :-
    !, write(Ask), write(' pan/i '), write(What), write('? [-100,100]|y|n\n'),

    string_codes("-_0123456789", AdditionalWordCars),
    readln([Resp], _, [10], AdditionalWordCars, lowercase),

    user_command(Resp, Goal, Stack),

    validate_true_false_response(Resp, CF) -> (
        assertz(dynamic_cache(Ask, What, CF)),
        assertz(dynamic_cache_single([Ask, What, CF]))
    ); is_true_false(Ask, What, CF, Goal, Stack).

validate_true_false_response(InCF, OutCF):-
    number(InCF),
    InCF =< 100, InCF >= -100,
    InCF = OutCF.

validate_true_false_response(Resp, 100):-
    sub_string(Resp, 0, _, _, 'y').

validate_true_false_response(Resp, -100):-
    sub_string(Resp, 0, _, _, 'n').

%% ============================================================================
%% asking for day-of-week
%% ============================================================================

is_day_of_week(Day, _, _) :-
    dynamic_cache(day, _), !, dynamic_cache(day, Day).

is_day_of_week(Day, Goal, Stack) :-
    !, write('jaki jest dzisiaj dzien tygodnia?\n'),
    write('[poniedzialek/wtorek/sroda/czwartek/piatek/sobota/niedziela]\n'),
    readln([Resp]),
    user_command(Resp, Goal, Stack),
    validate_day_of_week(Resp) -> (
        assertz(dynamic_cache(day, Resp)),
        assertz(dynamic_cache_single([day, Resp])),
        Day = Resp
    ); is_day_of_week(Day, Goal, Stack).

validate_day_of_week(Day) :-
    member(Day, [
        poniedzialek,
        wtorek,
        sroda,
        czwartek,
        piatek,
        sobota,
        niedziela
    ]).

%% ============================================================================
%% asking for religion
%% ============================================================================

is_religion(Religion, _, _) :-
    dynamic_cache(religion, _), !, dynamic_cache(religion, Religion).

is_religion(Religion, Goal, Stack) :-
    !, write('jaka religie wyznajesz?\n'),
    write('[chrzescijanin/zyd/muzulmanin/brak]\n'),
    readln([Resp]),
    user_command(Resp, Goal, Stack),
    validate_religion(Resp) -> (
        assertz(dynamic_cache(religion, Resp)),
        assertz(dynamic_cache_single([religion, Resp])),
        Religion = Resp
    ); is_religion(Religion, Goal, Stack).

validate_religion(Religion) :-
    member(Religion, [
        chrzescijanin,
        zyd,
        muzulmanin,
        brak
    ]).

%% ============================================================================
%% handling extended commands: why/what/how
%% ============================================================================

user_command(why, Goal, Stack) :-
  ext_why(Goal, Stack).

user_command(what, Goal, Stack) :-
  ext_what(false, Goal, Stack).

user_command(how, Goal, Stack) :-
  ext_what(true, Goal, Stack).

user_command(_, _, _).
  % passthrough

log_user_command(Cmd, Goal, Stack) :-
  write('COMD '), write(Cmd), nl,
  write('GOAL '), write(Goal), nl,
  write('STAK '), write(Stack), nl.

%% ============================================================================
%% command: why
%% ============================================================================

ext_why(Goal, Stack) :-
  write('\t'), write(Goal), nl,
  dump_why(Stack).

dump_why([ rule((Goal :- Premise), CF_rule)  |T]) :-
  write('\t\t'),
  write(Goal), write(' :: '), write(CF_rule), nl,
  % write('\t\t\t'), write(Premise), nl,
  dump_why_prem(Premise),
  dump_why(T).

dump_why(_).

dump_why_prem((Prem1, Prem2)) :- !,
  write('\t\t\t'), write(Prem1), nl,
  dump_why_prem(Prem2).

dump_why_prem(Prem) :- !,
  write('\t\t\t'), write(Prem), write('\n').

%% ============================================================================
%% command: what/how
%% ============================================================================

ext_what(ShowHow, _Goal, _Stack) :-
  write('\tSYMPTOMY ----------------------------------------------\n'),
  find_dynamic_cache(Dynamic),
  dump_dynamic_cache(Dynamic),
  write('\tFAKTY POSREDNIE ---------------------------------------\n'),
  find_known_rules(Rules),
  dump_known_rules(Rules, ShowHow),
  nl.

% -----------------------------------------------------------------------------
find_dynamic_cache(Rules) :-
  findall(X, dynamic_cache_single(X), Rules).

dump_dynamic_cache([]).

dump_dynamic_cache([ [X,Y,Z] | Rest]) :-
  write('\t\t'),
  write(X), write(' '), write(Y),
  write(': '), write(Z), write('\n'),
  dump_dynamic_cache(Rest).

dump_dynamic_cache([ [X,Z] | Rest]) :-
  write('\t\t'),
  write(X),
  write(': '), write(Z), write('\n'),
  dump_dynamic_cache(Rest).

% -----------------------------------------------------------------------------

find_known_rules(Rules) :-
  findall(X, known_rules_single(X), Rules).

dump_known_rules([]).

dump_known_rules([ [Goal, Premise, CF] | Rest ], ShowHow) :-
  write('\t\t'),
  write(Goal), write(' :: '), write(CF), nl,
  dump_known_rules_premise(ShowHow, Premise),
  dump_known_rules(Rest, ShowHow).

dump_known_rules_premise(true, Premise) :-
  dump_why_prem(Premise).

dump_known_rules_premise(_, _).
