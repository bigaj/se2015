
:- module(util, [
    is_age/1,
    is_true/2,
    is_false/2,
    is_day_of_week/1,
    is_religion/1
]).

:- dynamic([
    dynamic_cache/2,
    dynamic_cache/3,
    dynamic_cache_single/1,
    known_rules/2,
    known_rules_single/1
]).

cleanup() :-
    retractall(dynamic_cache(_, _)),
    retractall(dynamic_cache(_, _, _)),
    retractall(dynamic_cache_single(_)),
    retractall(known_rules(_, _)),
    retractall(known_rules_single(_, _)).

% age -------------------------------------------------------------------------

% asks user for age and checks if it is equal to Age
is_age(Age) :-
    dynamic_cache(age, _), !, dynamic_cache(age, Age).

is_age(Age) :-
    !, write('podaj wiek [stary/mlody/dorosly]\n'),
    readln([Resp]),
    user_command(Resp),
    member(Resp, [stary, mlody, dorosly]) -> (
        assertz(dynamic_cache(age, Resp)),
        assertz(dynamic_cache_single([age, Resp])),
        Age = Resp
    ); is_age(Age).

% generic y/n -----------------------------------------------------------------

is_true(Ask, What) :- is_true_false(Ask, What, true).
is_false(Ask, What) :- is_true_false(Ask, What, false).

is_true_false(Ask, What, Expected) :-
    dynamic_cache(Ask, What, _),         % if not stored already, goto next one
    !,                                   % or cut ...
    dynamic_cache(Ask, What, Expected).  % and check if expected val is stored

is_true_false(Ask, What, Expected) :-
    !, write(Ask), write(' pan/i '), write(What), write('? [y/n]\n'),
    readln([Resp]),
    user_command(Resp),
    validate_true_false_response(Resp, InputTrueOrFalse) -> (
        assertz(dynamic_cache(Ask, What, InputTrueOrFalse)),
        assertz(dynamic_cache_single([Ask, What, InputTrueOrFalse])),
        Expected = InputTrueOrFalse
    ); is_true_false(Ask, What, Expected).

validate_true_false_response(Resp, true):-
    sub_string(Resp, 0, _, _, 'y').

validate_true_false_response(Resp, false):-
    sub_string(Resp, 0, _, _, 'n').

% user commands ---------------------------------------------------------------

user_command(why) :-
  ext_why().

user_command(what) :-
  ext_what(false).

user_command(how) :-
  ext_what(true).

user_command(_).
  % passthrough

% day of week -----------------------------------------------------------------

is_day_of_week(Day) :-
    dynamic_cache(day, _), !, dynamic_cache(day, Day).

is_day_of_week(Day) :-
    !, write('jaki jest dzisiaj dzien tygodnia?\n'),
    write('[poniedzialek/wtorek/sroda/czwartek/piatek/sobota/niedziela]\n'),
    readln([Resp]),
    user_command(Resp),
    validate_day_of_week(Resp) -> (
        assertz(dynamic_cache(day, Resp)),
        assertz(dynamic_cache_single([day, Resp])),
        Day = Resp
    ); is_day_of_week(Day).

validate_day_of_week(Day) :-
    member(Day, [
        poniedzialek,
        wtorek,
        sroda,
        czwartek,
        piatek,
        sobota,
        niedziela
    ]).

% religion --------------------------------------------------------------------

is_religion(Religion) :-
    dynamic_cache(religion, _), !, dynamic_cache(religion, Religion).

is_religion(Religion) :-
    !, write('jaka religie wyznajesz?\n'),
    write('[chrzescijanin/zyd/muzulmanin/brak]\n'),
    readln([Resp]),
    user_command(Resp),
    validate_religion(Resp) -> (
        assertz(dynamic_cache(religion, Resp)),
        assertz(dynamic_cache_single([religion, Resp])),
        Religion = Resp
    ); is_religion(Religion).

validate_religion(Religion) :-
    member(Religion, [
        chrzescijanin,
        zyd,
        muzulmanin,
        brak
    ]).

% why -------------------------------------------------------------------------

ext_why() :-
    get_prolog_backtrace(100, Trace),
    maplist(map_stack2, Trace, MappedTrace),
    include(filter_stack, MappedTrace, FilteredTrace),
    dump_why(FilteredTrace).

dump_why([ [Pred, _Clause, _Head, _Body, Line, File]  |T]) :-
  write('\t'),
  write(Pred), write(' -> '), write(File), write('#'), write(Line),
  write('\n'),
  dump_why(T).

dump_why(_).

%% map_stack2(+Frame, -X).
%%
%% Extracts predicate name from stack frame, e.g.
%% frame(_, _, _, rules:eats(meat)) -> rules:eats(meat)
%%
%% @param Frame stackframe obtained from get_prolog_backtrace/2
%% @param Out   Output term

map_stack2(Frame, Out) :-
    Frame =.. [_, _Idx, WrappedClause, Pred],
    WrappedClause =.. [_, Clause, _],
    clause(Head, Body, Clause),
    clause_property(Clause, line_count(Line)),
    clause_property(Clause, file(File)),
    % write('\n'),
    Out = [Pred, Clause, Head, Body, Line, File].

%% filter_stack(+PredTerm).
%%
%% Filters out frames not related to the business logic.
%%
%% @param PredTerm term obtained from map_stack2/2

filter_stack([PredTerm, _, _, _, _, _]) :-
    PredTerm =.. Expanded,
    filter_stack_predicate(Expanded).

filter_stack_predicate([:, Package | Predicate]) :-
    Predicate =.. [_, Exp | _],
    Exp =.. [Name | _],
    filter_stack_predicate_accept(Package, Name).

filter_stack_predicate_accept(util, is_true_false).
filter_stack_predicate_accept(util, is_religion).
filter_stack_predicate_accept(util, is_day_of_week).
filter_stack_predicate_accept(util, is_age).
filter_stack_predicate_accept(rules, eats_impl).
filter_stack_predicate_accept(rules, allowed_by_religion).
filter_stack_predicate_accept(rules, suggest_food).

% what ------------------------------------------------------------------------

store_fact(Clause, Data) :-
  not(known_rules(Clause, Data)),
  assertz(known_rules(Clause, Data)),
  assertz(known_rules_single([Clause, Data])),

store_fact(_, _).

ext_what(ShowHow) :-
    write('SYMPTOMY ------------------------------------------------------\n'),
    find_dynamic_cache(Dynamic),
    dump_dynamic_cache(Dynamic),
    % listing(dynamic_cache_single),
    % % listing(known_rules),
    write('FAKTY POSREDNIE -----------------------------------------------\n'),
    find_known_rules(Rules),
    dump_known_rules(Rules, ShowHow).

find_known_rules(Rules) :-
  findall(X, known_rules_single(X), Rules).

dump_known_rules([]).

dump_known_rules([ [Clause, Body] | Rest ], ShowHow) :-
  write('\t'),
  write(Clause),
  write(':\n'),
  try_expanding(Body, Expanded),
  (ShowHow -> dump_rule_body(Expanded); true),
  dump_known_rules(Rest, ShowHow).

try_expanding(Term, Out) :-
  compound_name_arity(Term, Name, _),
  % expand_term(Name, Term, Out).
  (Name = ',' ->
    (Term =.. [_ | Out]);
    ([Term] = Out)
  ).

find_dynamic_cache(Rules) :-
  findall(X, dynamic_cache_single(X), Rules).

dump_dynamic_cache([]).

dump_dynamic_cache([ [X,Y,Z] | Rest]) :-
  write('\t'),
  write(X), write(' '), write(Y),
  write(': '), write(Z), write('\n'),
  dump_dynamic_cache(Rest).

dump_dynamic_cache([ [X,Z] | Rest]) :-
  write('\t'),
  write(X),
  write(': '), write(Z), write('\n'),
  dump_dynamic_cache(Rest).

% expand_term(',', Term, Out) :- Term =.. [_ | Out].
% expand_term(_, Term, Out) :- [Term] = Out.

dump_rule_body([]).

dump_rule_body([H|T]) :-
  write('\t\t'),
  write(H),
  write('\n'),
  dump_rule_body(T).
