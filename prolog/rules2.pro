
:- module(rules, [
  rule/2,
  askable/1
]).

use_module('util2').

%% ============================================================================
%% dishes
%% ============================================================================

rule((
  suggest_food(schabowy) :-
      eats_impl(pork),
      eats_impl(fried),
      eats_impl(full),
      not(is_true('czy jest', 'w pospiechu'))
  ), 100).

rule((
  suggest_food(hamburger) :-
      is_true('czy jest', 'w pospiechu'),
      eats_impl(fried),
      eats_impl(beef)
  ), 100).

rule((
  suggest_food('vege schabowy') :-
      eats(fried)
  ), 100).


rule((
  suggest_food('kurczak po tajsku') :-
      eats_impl(asian),
      eats_impl(meat),
      eats_impl(spicy)
  ), 100).

rule((
  suggest_food(spaghetti) :-
      eats_impl(beef),
      eats_impl(pasta)
  ), 100).

rule((
  suggest_food('pierogi z owocami') :-
      eats_impl(sweet),
      eats_impl(boiled)
  ), 100).

rule((
  suggest_food('nalesniki z serem') :-
      eats_impl(sweet),
      eats_impl(fried)
  ), 100).

rule((
  suggest_food('pizza z krewetkami') :-
      eats_impl(seafood),
      not(is_true('czy jest', 'uczulony na gluten'))
  ), 100).

rule((
  suggest_food('pizza diabolo') :-
      eats_impl(meat),
      eats_impl(spicy),
      not(is_true('czy jest', 'uczulony na gluten'))
  ), 100).

rule((
  suggest_food('ryba i frytki') :-
      eats_impl(fried),
      eats_impl(light)
  ), 100).

rule((
  suggest_food('chilli con carne') :-
      eats_impl(pork),
      eats_impl(rice),
      eats_impl(spicy)
  ), 100).

rule((
  suggest_food('ryz z warzywami') :-
      eats_impl(boiled),
      eats_impl(rice),
      eats_impl(fit)
  ), 100).

rule((
  suggest_food('salatka warzywna i owocami morza') :-
      eats_impl(fit),
      eats_impl(seafood)
  ), 100).

%% ============================================================================
%% rules
%% ============================================================================

rule((
  eats_impl(pork) :-
      eats_impl(meat),
      not(is_religion(muzulmanin))
  ), 95).

rule((
  eats_impl(beef) :-
      eats_impl(meat)
  ), 95).

rule((
  eats_impl(meat) :-
      not(is_true('czy jest', wegetarianin)),
      allowed_by_religion(meat)
  ), 95).

rule((
  eats_impl(fried) :-
      is_true('czy lubi', 'smazone potrawy'),
      not(is_true('czy ma', 'podwyzszony cholesterol'))
  ), 95).

rule((
  eats_impl(boiled) :-
      is_true('czy lubi', 'gotowane potrawy')
  ), 95).

rule((
  eats_impl(sweet) :-
      not(is_true('czy jest', 'uczulony na cukier'))
  ), 95).

rule((
  eats_impl(asian) :-
      is_true('czy lubi', 'kuchnie azjatycka'),
      not(is_true('czy jest', 'uczulony na sos sojowy')),
      eats_impl(fried),
      eats_impl(rice)
  ), 95).

rule((
  eats_impl(spicy) :-
      is_true('czy lubi', 'ostre potrawy'),
      not(is_age(mlody))
  ), 95).

rule((
  eats_impl(full) :-
      is_true('czy jest', 'bardzo glodny'),
      not(is_age(mlody)),
      is_true('czy jest', mezczyzna)
  ), 95).

rule((
  eats_impl(light) :-
      is_true('czy jest', 'na diecie');
      is_true('czy chce', 'lekki posilek')
  ), 95).

rule((
  eats_impl(fit) :-
      not(is_true('czy jest', mezczyzna)),
      eats_impl(light)
  ), 95).

rule((
  eats_impl(pasta) :-
      not(is_true('czy jest', 'uczulony na gluten')),
      eats_impl(boiled)
  ), 95).

rule((
  eats_impl(rice) :-
      not(is_true('czy jest', 'uczulony na gluten')),
      eats_impl(boiled),
      is_true('czy lubi', 'dania z ryzem')
  ), 95).

rule((
  eats_impl(seafood) :-
      is_true('czy lubi', 'kuchnie srodziemnomorska'),
      is_true('czy chce', 'lekki posilek')
  ), 95).

rule((
  allowed_by_religion(meat) :-
      % is_religion(chrzescijanin), !, % not supported by the evaluator
      not(is_day_of_week(piatek))
  ), 95).

rule((
  allowed_by_religion(_)
  ), 95).

%% ============================================================================
%% meta-info for the evaluator
%% ============================================================================

askable(is_true(_,_)).
askable(is_religion(_)).
askable(is_day_of_week(_)).
askable(is_age(_)).
