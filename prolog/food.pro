
:- module(food,[
    main/0
]).

use_module('rules').

cleanup() :-
    util:cleanup().

main() :-
    suggest_food(Food), !,
    write('Proponuje panu/i '), write(Food), write('\n'),
    cleanup().

main() :-
    write('Niestety zadna potrawa nie jest w stanie\n'),
    write('sprostac pana/i oczekiwaniom.\n'),
    cleanup().
