
:- module(rules, [
  suggest_food/1
]).

use_module('util').

% dishes ---------------------------------------------------------------------

suggest_food(schabowy) :-
    eats(pork),
    eats(fried),
    eats(full),
    is_false('czy jest', 'w pospiechu').

suggest_food(hamburger) :-
    is_true('czy jest', 'w pospiechu'),
    eats(fried),
    eats(beef).

suggest_food('vege schabowy') :-
    eats(fried).

suggest_food('kurczak po tajsku') :-
    eats(asian),
    eats(meat),
    eats(spicy).

suggest_food(spaghetti) :-
    eats(beef),
    eats(pasta).

suggest_food('pierogi z owocami') :-
    eats(sweet),
    eats(boiled).

suggest_food('nalesniki z serem') :-
    eats(sweet),
    eats(fried).

suggest_food('pizza z krewetkami') :-
    eats(seafood),
    is_false('czy jest', 'uczulony na gluten').

suggest_food('pizza diabolo') :-
    eats(meat),
    eats(spicy),
    is_false('czy jest', 'uczulony na gluten').

suggest_food('ryba i frytki') :-
    eats(fried),
    eats(light).

suggest_food('chilli con carne') :-
    eats(pork),
    eats(rice),
    eats(spicy).

suggest_food('ryz z warzywami') :-
    eats(boiled),
    eats(rice),
    eats(fit).

suggest_food('salatka warzywna i owocami morza') :-
    eats(fit),
    eats(seafood).

% rules ----------------------------------------------------------------------

eats(What) :-
  Result = eats_impl(What),
  clause(eats_impl(What), Body),
  Result,
  util:store_fact(eats_impl(What), Body).
  % (Result ->
  %   util:store_fact(eats_impl(What), Body); true),
  % Result.

eats_impl(pork) :-
    eats(meat),
    not(is_religion(muzulmanin)).

eats_impl(beef) :-
    eats(meat).

eats_impl(meat) :-
    is_false('czy jest', wegetarianin),
    allowed_by_religion(meat).

eats_impl(fried) :-
    is_true('czy lubi', 'smazone potrawy'),
    is_false('czy ma', 'podwyzszony cholesterol').

eats_impl(boiled) :-
    is_true('czy lubi', 'gotowane potrawy').

eats_impl(sweet) :-
    is_false('czy jest', 'uczulony na cukier').

eats_impl(asian) :-
    is_true('czy lubi', 'kuchnie azjatycka'),
    is_false('czy jest', 'uczulony na sos sojowy'),
    eats(fried),
    eats(rice).

eats_impl(spicy) :-
    is_true('czy lubi', 'ostre potrawy'),
    not(is_age(mlody)).

eats_impl(full) :-
    is_true('czy jest', 'bardzo glodny'),
    not(is_age(mlody)),
    is_true('czy jest', mezczyzna).

eats_impl(light) :-
    is_true('czy jest', 'na diecie');
    is_true('czy chce', 'lekki posilek').

eats_impl(fit) :-
    is_false('czy jest', mezczyzna),
    eats(light).

eats_impl(pasta) :-
    is_false('czy jest', 'uczulony na gluten'),
    eats(boiled).

eats_impl(rice) :-
    is_false('czy jest', 'uczulony na gluten'),
    eats(boiled),
    is_true('czy lubi', 'dania z ryzem').

eats_impl(seafood) :-
    is_true('czy lubi', 'kuchnie srodziemnomorska'),
    is_true('czy chce', 'lekki posilek').

allowed_by_religion(meat) :-
    is_religion(chrzescijanin), !,
    not(is_day_of_week(piatek)).

allowed_by_religion(_).
