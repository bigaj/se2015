
:- module(food,[
    main/0
]).

use_module('rules').

main() :-
    solve(suggest_food(Food), CF), !,
    write('Proponuje panu/i '), write(Food), write('\n'),
    write('Z pewnoscia '), write(CF), write(' %\n').

main() :-
    write('Niestety zadna potrawa nie jest w stanie\n'),
    write('sprostac pana/i oczekiwaniom.\n').
